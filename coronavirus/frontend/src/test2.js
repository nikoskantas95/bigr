import {PolymerElement,html} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';

class HelloWorld extends PolymerElement {

    static get template() {
        return html` <body>
        <p>This is an example of a simple HTML page with one paragraph.</p>
          <div id="regions_div" style="width: 900px; height: 500px;"></div>
        </body>
        <head>
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
          <script type="text/javascript">
            google.charts.load('current', {
              'packages':['geochart'],
              // Note: you will need to get a mapsApiKey for your project.
              // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
              'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
            });
            google.charts.setOnLoadCallback(drawRegionsMap);
      
            function drawRegionsMap() {
              var data = google.visualization.arrayToDataTable([
                ['Country', 'Popularity'],
                ['Germany', 200],
                ['United States', 300],
                ['Brazil', 400],
                ['Canada', 500],
                ['France', 600],
                ['RU', 700],
                ['Greece',1000]
              ]);
      
              var options = {};
      
              var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
      
              chart.draw(data, options);
            }
          </script>
        </head>
        <body>
          <div id="regions_div" style="width: 900px; height: 500px;"></div>
          <p>This is thye eeeeeeeeeeeeeeeeeeeeend.</p>
        </body>`;
    }

    static get is() {
          return 'hello-world';
    }
}

customElements.define(HelloWorld.is, HelloWorld);