package com.packagename.corona.components;

import com.packagename.corona.models.Statistic;
import com.packagename.corona.services.DbService;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.*;
import lombok.NoArgsConstructor;

import javax.inject.Inject;

@NoArgsConstructor
public class StatisticPieChart extends Chart {
    private Configuration configuration = this.getConfiguration();

    @Inject
    private DbService dbService;


    public StatisticPieChart(String country) {
        Statistic current = dbService.getCountryStatistic(country);

        Chart chart = new Chart(ChartType.PIE);
        Configuration conf = chart.getConfiguration();
        conf.setTitle("Epidemic Statistics for " + country);
        Tooltip tooltip = new Tooltip();
        tooltip.setValueDecimals(1);
        conf.setTooltip(tooltip);

        PlotOptionsPie plotOptions = new PlotOptionsPie();
        plotOptions.setAllowPointSelect(true);
        plotOptions.setCursor(Cursor.POINTER);
        plotOptions.setShowInLegend(true);
        conf.setPlotOptions(plotOptions);

        DataSeries series = new DataSeries();
        long aux = current.getConfirmed() - current.getDeaths() - current.getRecovered();
        DataSeriesItem active = new DataSeriesItem("Active cases", aux);
        active.setSelected(true);
        active.setSliced(true);
        series.add(active);
        series.add(new DataSeriesItem("Recovered cases", current.getRecovered()));
        series.add(new DataSeriesItem("Total deaths", current.getDeaths()));

        conf.setSeries(series);
        chart.setVisibilityTogglingDisabled(true);
    }
}
