package com.packagename.corona.components;

import javax.annotation.PostConstruct;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;


public class TestDiv extends Div {

  @PostConstruct
    public void init(){
      UI.getCurrent().getPage().addHtmlImport("test.html");
  }
}
