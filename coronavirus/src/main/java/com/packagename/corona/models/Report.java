package com.packagename.corona.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Reports")
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date date;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private Country country;

    @OneToOne(cascade = {CascadeType.ALL})
    private Statistic statistic;

    @SneakyThrows
    public Report(String dt) {
        String clean_date = dt.substring(0, 10);
        if (clean_date.contains("-")) {
            this.date = new SimpleDateFormat("MM-dd-yyy").parse(clean_date);
        }
        else{
            clean_date=clean_date.replaceAll("","-");
        }
    }

    public String toString() {
        //  String str = new SimpleDateFormat("yyy-MM-dd").p
        return "Date: " + this.date.toString();
    }


}
