package com.packagename.corona.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class CsvModel {
    private String province;
    private String country;
    private Long confirmed;
    private Long recovered;
    private Long deaths;
}
