package com.packagename.corona.models;

public enum StatisticType {
    CONFIRMED,
    DEATHS,
    RECOVERED
}
