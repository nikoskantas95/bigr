package com.packagename.corona.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
public class WorldStatistic {

   // private Date date;

    private Long confirmed;

    private Long deaths;

    private Long recovered;

    private Long active;

    public static WorldStatistic  fromStatistic(Statistic statistic){
        return new WorldStatistic(statistic.getConfirmed(),
            statistic.getDeaths(),statistic.getRecovered(),
            statistic.getConfirmed()-statistic.getRecovered()-statistic.getDeaths());
    }

}
