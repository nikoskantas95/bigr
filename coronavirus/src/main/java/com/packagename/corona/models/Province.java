package com.packagename.corona.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Provincies")
public class Province {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String country;

    @Column(nullable = false)
    private double longitude;

    @Column(nullable = false)
    private double latitude;

    @OneToMany(mappedBy = "country",fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    private List<Report> reports=new ArrayList<>();

    public Province(String name, String country, String latitude, String longitude){
        //(!confirmed.equals("")) ? Long.valueOf(confirmed) : Long.valueOf(0);
        this.name=name;
        this.country=country;
        this.latitude=(!latitude.equals("")) ? Double.parseDouble(latitude) : 0;
        this.longitude=(!longitude.equals("")) ? Double.parseDouble(longitude) : 0;
    }

    @Override
    public String toString() {
        return "Province{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}

