package com.packagename.corona.models;

import com.packagename.corona.util.Manifest;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ProblematicChecker {
    private ArrayList<Country> problematicCountries = new ArrayList<>();

    public void addProblematicCountry(Country country) {
        problematicCountries.add(country);
    }

    public ProblematicType isProblematic(String country) {
        if (Manifest.getProblematicCountries().contains(country))
            return ProblematicType.PROBLEMATIC;
        else
            return ProblematicType.NON;
    }

    public boolean containsProblematicCountry(String name) {
        for (Country country : problematicCountries) {
            if (country.getCountry().equals(name))
                return true;
        }
        return false;
    }

    public Country getProblematicCountry(String name) {
        Country country = null;
        for (Country item : problematicCountries) {
            if (item.getCountry().equals(name)) {
                country = item;
                break;

            }

        }
        return country;
    }
}
