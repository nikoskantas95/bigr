package com.packagename.corona.models;

import com.vaadin.flow.component.html.Image;
import lombok.Getter;

@Getter
public class WorldStatisticWrapper extends WorldStatistic{
    private Country country;

    private String name;

    private Image image = new Image("C:/Users/Nikos/Documents/shared/Ptyxiaki/flags/gr.png","");

    public WorldStatisticWrapper(Statistic statistic) {
        super(statistic.getConfirmed(),statistic.getDeaths(),statistic.getRecovered(),
            statistic.getConfirmed()-statistic.getDeaths()-statistic.getRecovered());
        this.country=statistic.getReport().getCountry();
        this.name = country.getCountry();
    }
}
