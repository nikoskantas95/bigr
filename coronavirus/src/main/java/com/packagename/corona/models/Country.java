package com.packagename.corona.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
@Entity
public class Country {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String country;

    @OneToMany(mappedBy = "country",fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    private List<Report> reports=new ArrayList<>();

    public Country(String country){
        this.country=country;
    }
}
