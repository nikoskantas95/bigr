package com.packagename.corona.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Statistics",schema = "coronavirus")
public class Statistic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long confirmed;

    @Column
    private Long deaths;

    @Column
    private Long recovered;

    @OneToOne(cascade = {CascadeType.MERGE})
    Report report;

    public Statistic(String confirmed, String deaths, String recovered) {

        this.confirmed = (!confirmed.equals("")) ? Long.valueOf(confirmed) : Long.valueOf(0);
        this.deaths = (!deaths.equals("")) ? Long.valueOf(deaths) : Long.valueOf(0);
        this.recovered = (!recovered.equals("")) ? Long.valueOf(recovered) : Long.valueOf(0);
    }

    public Statistic(Long confirmed,Long deaths,Long recovered){
        this.confirmed=confirmed;
        this.deaths=deaths;
        this.recovered=recovered;
    }

    public String toString() {
        return "Confirmed: " + this.confirmed.toString() + " Deaths: " + this.deaths.toString() + " Recovered: " + this.recovered.toString();
    }
}
