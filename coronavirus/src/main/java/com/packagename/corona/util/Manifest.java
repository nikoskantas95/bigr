package com.packagename.corona.util;

import java.util.ArrayList;
import java.util.List;

import com.packagename.corona.models.Country;

public class Manifest {

    private static Manifest instance;
    private static List<String> countries = new ArrayList<>();
    private static List<String> problematicCountries = new ArrayList<>();

    private static Manifest getInstance() {
        if (instance == null) {
            synchronized (Manifest.class) {
                if (instance == null) { // yes double check
                    instance = new Manifest();
                }
            }
        }
        return instance;
    }

    public static void update(String country){
        if(getInstance().countries.contains(country) && !getInstance().problematicCountries.contains(country)){
            getInstance().problematicCountries.add(country);
            System.out.println("Total problematic countries: "+getInstance().problematicCountries.size());
            System.out.println(getInstance().problematicCountries);
        }
        else if(!getInstance().countries.contains(country)){
            getInstance().countries.add(country);
        }
    }

    public static List<String> getProblematicCountries(){
        return getInstance().problematicCountries;
    }

    public static void renew(){
        getInstance().countries.clear();
    }
}
