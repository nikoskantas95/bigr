package com.packagename.corona.services;

import com.packagename.corona.models.*;
import lombok.var;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class DbService {

    @PersistenceContext
    EntityManager em;

    @PostConstruct
    void init() {
    }


    @Transactional
    public Statistic getWorldStatistic() {
        Date current = getYesterdayDate();


        List<Report> reports = em.createQuery("SELECT r FROM Report r WHERE r.date=:date")
            .setParameter("date", current)
            .getResultList();

        if (reports == null) {
            System.out.println("!!!");
        }

        Long total = 0L, recovered = 0L, dead = 0L;
        for (Report report : reports) {
            total += report.getStatistic().getConfirmed();
            recovered += report.getStatistic().getRecovered();
            dead += report.getStatistic().getDeaths();
        }
        return new Statistic(total, dead, recovered);
    }

    @Transactional
    public List<Country> getCountriesList() {
        return em.createQuery("SELECT c FROM Country c")
            .getResultList();
    }


    @Transactional
    public Province getProvince(String province, String country) {
        Province result;
        // System.out.println("Searching for "+country + " " +province);
        // its a province
        if (!province.equals("")) {
            try {
                result = (Province) em.createQuery("SELECT p FROM Province p WHERE p.name=:province")
                    .setParameter("province", province).setMaxResults(1)
                    .getSingleResult();
            } catch (NoResultException e) {
                result = null;
            }
        }
        //country without provincies
        else {
            try {
                result = (Province) em.createQuery("SELECT p FROM Province p WHERE p.country=:country")
                    .setParameter("country", country).setMaxResults(1)
                    .getSingleResult();
            } catch (NoResultException e) {
                result = null;
            }
        }

        return result;
    }

    @Transactional
    public Statistic getCountryStatistic(String country) {
        Country province = (Country) em.createQuery("SELECT c FROM Country c WHERE c.country=:country")
            .setParameter("country", country)
            .getSingleResult();

        return province.getReports().get(province.getReports().size() - 1).getStatistic();
    }

    @Transactional
    public Statistic[] getWeeklyStatistics(String country) {

        List<Report> reports = em.createQuery("SELECT r FROM Report r WHERE r.country.country=:country ORDER BY r.date DESC")
            .setParameter("country", country)
            .setMaxResults(7)
            .getResultList();

        Statistic[] list = new Statistic[7];
        int index = 6;
        for (Report report : reports) {
            list[index] = report.getStatistic();
            index--;
        }


        return list;

    }

    @Transactional
    public List<Report> getCountryReports(String country) {
        return em.createQuery("SELECT r FROM Report r WHERE r.country.country=:country")
            .setParameter("country", country)
            .getResultList();

    }


    private Date getYesterdayDate() {
        Calendar current = new GregorianCalendar();
        current.set(Calendar.HOUR_OF_DAY, 0);
        current.set(Calendar.MINUTE, 0);
        current.set(Calendar.SECOND, 0);
        current.set(Calendar.MILLISECOND, 0);
        current.add(Calendar.DAY_OF_MONTH, -1);

        return current.getTime();
    }

    private Date getBeforeYesterdayDate() {
        Calendar current = new GregorianCalendar();
        current.set(Calendar.HOUR_OF_DAY, 0);
        current.set(Calendar.MINUTE, 0);
        current.set(Calendar.SECOND, 0);
        current.set(Calendar.MILLISECOND, 0);
        current.add(Calendar.DAY_OF_MONTH, -2);

        return current.getTime();
    }

    @Transactional
    public Country getCountry(String country) {
        Country result;
        try {
            result = (Country) em.createQuery("SELECT c FROM Country c WHERE c.country=:country")
                .setParameter("country", country)
                .getSingleResult();
        } catch (NonUniqueResultException | NoResultException e) {
            result = null;
        }
        return result;

    }

    @Transactional
    public List<Statistic> getMostHealthy() {
        List<Country> list = getCountriesList();
        List<Statistic> statisticList = new ArrayList<>();
        for (Country c : list) {
            statisticList.add(c.getReports().get(c.getReports().size() - 1).getStatistic());
        }
        statisticList.sort(Comparator.comparingDouble(Statistic::getConfirmed));
        return statisticList.subList(0, 10);

    }

    @Transactional
    public List<Statistic> getMostAffected() {
        List<Country> list = getCountriesList();
        List<Statistic> statisticList = new ArrayList<>();
        for (Country c : list) {
            statisticList.add(c.getReports().get(c.getReports().size() - 1).getStatistic());
        }
        statisticList.sort(Comparator.comparingDouble(Statistic::getConfirmed));
        return statisticList.subList(statisticList.size() - 10, statisticList.size());
    }

    @Transactional
    public HashMap<String, Long> mostDeathsLastDays() {
        Date yesterday = getYesterdayDate();
        Date twoDaysAgo = getBeforeYesterdayDate();
        HashMap<String, Long> result = new HashMap<>();

        List<Report> reports = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.deaths DESC")
            .setParameter("date", yesterday)
            // .setMaxResults(3)
            .getResultList();

        HashMap<String, Long> map1 = new HashMap<>();
        reports.forEach(e -> {
            map1.put(e.getCountry().getCountry(), e.getStatistic().getDeaths());
        });

        List<Report> reports2 = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.deaths DESC")
            .setParameter("date", twoDaysAgo)
            // .setMaxResults(3)
            .getResultList();

        HashMap<String, Long> map2 = new HashMap<>();
        reports2.forEach(e -> {

            map2.put(e.getCountry().getCountry(),
                map1.get(e.getCountry().getCountry()) - e.getStatistic().getDeaths());
        });

        return findTopThree(map2);
    }

    @Transactional
    public HashMap<String, Float> mostDeathsLastDaysPercentage() {
        Date yesterday = getYesterdayDate();
        Date twoDaysAgo = getBeforeYesterdayDate();

        List<Report> reports = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.deaths DESC")
            .setParameter("date", yesterday)
            .setMaxResults(3)
            .getResultList();

        HashMap<String, Long> map1 = new HashMap<>();
        reports.forEach(e -> {
            map1.put(e.getCountry().getCountry(), e.getStatistic().getDeaths());
        });

        List<Report> reports2 = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.deaths DESC")
            .setParameter("date", twoDaysAgo)
            .setMaxResults(3)
            .getResultList();

        HashMap<String, Float> map2 = new HashMap<>();
        reports2.forEach(e -> {
            float percentage = ((float) (map1.get(e.getCountry().getCountry()) - e.getStatistic().getDeaths()) / (float) e.getStatistic().getDeaths()) * 100;
            map2.put(e.getCountry().getCountry(), percentage);
        });
        return sortMap(map2);
    }

    @Transactional
    public HashMap<String, Long> mostConfirmedLastDays() {
        Date yesterday = getYesterdayDate();
        Date twoDaysAgo = getBeforeYesterdayDate();
        HashMap<String, Long> result = new HashMap<>();

        List<Report> reports = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.confirmed DESC")
            .setParameter("date", yesterday)
            // .setMaxResults(3)
            .getResultList();

        HashMap<String, Long> map1 = new HashMap<>();
        reports.forEach(e -> {
            map1.put(e.getCountry().getCountry(), e.getStatistic().getConfirmed());
        });

        List<Report> reports2 = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.confirmed DESC")
            .setParameter("date", twoDaysAgo)
            // .setMaxResults(3)
            .getResultList();

        HashMap<String, Long> map2 = new HashMap<>();
        reports2.forEach(e -> {

            map2.put(e.getCountry().getCountry(),
                map1.get(e.getCountry().getCountry()) - e.getStatistic().getConfirmed());
        });

        return findTopThree(map2);
    }

    @Transactional
    public HashMap<String, Float> mostConfirmedLastDaysPercentage() {
        Date yesterday = getYesterdayDate();
        Date twoDaysAgo = getBeforeYesterdayDate();

        List<Report> reports = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.confirmed DESC")
            .setParameter("date", yesterday)
            .setMaxResults(3)
            .getResultList();

        HashMap<String, Long> map1 = new HashMap<>();
        reports.forEach(e -> {
            map1.put(e.getCountry().getCountry(), e.getStatistic().getConfirmed());
        });

        List<Report> reports2 = em.createQuery("SELECT r FROM Report r  WHERE r.date=:date ORDER BY r.statistic.confirmed DESC")
            .setParameter("date", twoDaysAgo)
            .setMaxResults(3)
            .getResultList();

        HashMap<String, Float> map2 = new HashMap<>();
        reports2.forEach(e -> {
            float percentage = ((float) (map1.get(e.getCountry().getCountry()) - e.getStatistic().getConfirmed()) / (float) e.getStatistic().getConfirmed()) * 100;
            map2.put(e.getCountry().getCountry(), percentage);
        });
        return sortMap(map2);
    }


    private HashMap<String, Float> sortMap(HashMap<String, Float> map) {
        Float[] objects = map.values().toArray(new Float[0]);
        Arrays.sort(objects, Collections.reverseOrder());

        LinkedHashMap<String, Float> ordered = new LinkedHashMap<>();
        for (Float value : objects) {
            for (String key : map.keySet()) {
                if (map.get(key).equals(value)) {
                    ordered.put(key, value);
                }
            }
        }
        return ordered;
    }

    private HashMap<String, Long> findTopThree(HashMap<String, Long> map) {
        LinkedHashMap<String, Long> result = new LinkedHashMap<>();
        ArrayList<String> countries = new ArrayList<>(map.keySet());
        ArrayList<Long> confirmed = new ArrayList<>(map.values());

        for (int i = 0; i < 3; i++) {
            int index = confirmed.indexOf(Collections.max(confirmed));
            result.put(countries.get(index), confirmed.get(index));
            countries.remove(countries.get(index));
            confirmed.remove(confirmed.get(index));
        }

        return result;
    }


}
