package com.packagename.corona.services;

import com.opencsv.CSVReader;
import com.packagename.corona.models.*;
import com.packagename.corona.util.Manifest;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

public class AdminService {
    @PersistenceContext
    EntityManager em;

    @Inject
    private DbService dbService;


    public void createFromScratch() {
        createDB();
//        final String rawLocation = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports";
//
//        File f = new File(rawLocation);
//        ArrayList<String> files = new ArrayList<String>(Arrays.asList(f.list()));
//        files.remove(0);
//        files.remove(files.size()-1);
//        for(String file : files){
//            updateVirusDB(rawLocation+file);
        //    }

    }

    @Transactional
    private void createDB() {
        final String csvFile = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/03-17-2020.csv";

        CSVReader reader = null;
        Province province;
        Report report;
        Statistic statistic;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            line = reader.readNext();
//            while ((line = reader.readNext()) != null) {
//
//
//
////                province = new Province(line[1], line[0], Double.parseDouble(line[6]), Double.parseDouble(line[7]));
////                report = new Report(line[2]);
////                statistic = new Statistic(line[3], line[4], line[5]);
////
////                province.getReports().add(report);
////                report.setProvince(province);
////                report.setStatistic(statistic);
////                statistic.setReport(report);
////
////                em.persist(province);
////                em.persist(report);
////                em.persist(statistic);
//
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Transactional
    public void updateLastMonth() {
        final String rawLocation = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports";

        File f = new File(rawLocation);
        ArrayList<String> files = new ArrayList<String>(Arrays.asList(Objects.requireNonNull(f.list())));
        files.remove(0);
        files.remove(files.size() - 1);

        System.out.println(getCurrentMonth());
        for (String file : files) {
            if (!(file.startsWith(getCurrentMonth()))) {
                System.out.println("Skipped " + file);
                continue;
            }
            String data = rawLocation + "/" + file;
            // String loc = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/03-22-2020.csv";

            HashMap<String, Integer> indexMap = getMapping(data);
            ArrayList<CsvModel> csvData = parseData(data, indexMap);
            Manifest.renew();
            save(csvData, data);

        }
        System.out.println("Coronavirus for month " + getCurrentMonth() + "is up to date");


    }

    private String getCurrentMonth() {
        int month = Calendar.getInstance().get(Calendar.MONTH);
        if (month < 10) {
            return "0" + (month + 1);
        }
        return String.valueOf(month + 1);
    }

    //todo move location to resources
    @Transactional
    public void updateFromScratch() {
        final String rawLocation = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports";

        File f = new File(rawLocation);
        ArrayList<String> files = new ArrayList<String>(Arrays.asList(f.list()));
        files.remove(0);
        files.remove(files.size() - 1);
        //ArrayList<String> files=new ArrayList<>();
        //files.add("C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/03-17-2020.csv");
//        files.add("C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/03-26-2020.csv");
        for (String file : files) {
            if (!(file.startsWith("07-"))) {
                System.out.println("Skipped " + file);
                continue;
            }
            String data = rawLocation + "/" + file;
            // String loc = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/03-22-2020.csv";

            HashMap<String, Integer> indexMap = getMapping(data);
            ArrayList<CsvModel> csvData = parseData(data, indexMap);
            Manifest.renew();
            save(csvData, data);
            //persistEntities(data, indexMap);

        }
        System.out.println("Done");
    }

    private ArrayList<CsvModel> parseData(String path, HashMap<String, Integer> map) {
        ArrayList<CsvModel> csvData = new ArrayList<>();
        CSVReader reader = null;

        try {
            reader = new CSVReader(new FileReader(path));
            String[] line;
            line = reader.readNext();

            while ((line = reader.readNext()) != null) {
                if (line[map.get("Country")].contains("Diamond"))
                    continue;

                Manifest.update(line[map.get("Country")]);
                CsvModel model = new CsvModel();

                model.setCountry(line[map.get("Country")]);
                model.setProvince(line[map.get("Province")]);
                model.setConfirmed(Long.valueOf(line[map.get("Confirmed")]));
                model.setDeaths(Long.valueOf(line[map.get("Deaths")]));
                model.setRecovered(Long.valueOf(line[map.get("Recovered")]));

                csvData.add(model);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return csvData;
    }

    @Transactional
    private void save(ArrayList<CsvModel> data, String path) {
        ProblematicChecker problematicChecker = new ProblematicChecker();
        ArrayList<Country> nonProblematicCountries = new ArrayList<>();
        ArrayList<Country> problematicCountries = problematicChecker.getProblematicCountries();
        String date = path.substring(106, 116);
        System.out.println("Date: " + date);


        for (CsvModel model : data) {
            Country country;
            Report report;
            Statistic statistic;

            Country existedCountry = dbService.getCountry(model.getCountry());
            if (existedCountry != null) {
                country = existedCountry;
            } else {
                country = new Country(model.getCountry());
            }

            switch (problematicChecker.isProblematic(model.getCountry())) {
                case NON:
                    report = new Report(date);
                    statistic = new Statistic(model.getConfirmed(), model.getDeaths(), model.getRecovered());
                    country.getReports().add(report);
                    report.setCountry(country);
                    report.setStatistic(statistic);
                    statistic.setReport(report);
                    nonProblematicCountries.add(country);
                    // System.out.println("Saving " + country.getCountry() + " in DB...");
                    break;
                // em.merge(country);

                case PROBLEMATIC:
                    if (!problematicChecker.containsProblematicCountry(model.getCountry())) {
                        report = new Report(date);
                        statistic = new Statistic(model.getConfirmed(), model.getDeaths(), model.getRecovered());
                        country.getReports().add(report);
                        report.setCountry(country);
                        report.setStatistic(statistic);
                        statistic.setReport(report);
                        problematicCountries.add(country);
                    } else {
//                        if (existedCountry == null) {
                        country = problematicChecker.getProblematicCountry(model.getCountry());

                        Statistic problematicStatistic = country.getReports().get(country.getReports().size() - 1).getStatistic();

                        problematicStatistic.setConfirmed(problematicStatistic.getConfirmed() + model.getConfirmed());
                        problematicStatistic.setDeaths(problematicStatistic.getDeaths() + model.getDeaths());
                        problematicStatistic.setRecovered(problematicStatistic.getRecovered() + model.getRecovered());
                        //}

                    }
                    break;


            }

        }
        for (Country item : nonProblematicCountries) {
            em.merge(item);
        }
        for (Country item : problematicCountries) {
            em.merge(item);
        }
        System.out.println(problematicCountries.size());

    }


    private HashMap<String, Integer> getMapping(String data) {
        HashMap<String, Integer> map = new HashMap<>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(data));
            String[] line;
            line = reader.readNext();
            for (int i = 0; i < line.length; i++) {
                if (line[i].contains("Province"))
                    map.put("Province", i);
                else if (line[i].contains("Country"))
                    map.put("Country", i);
                else if (line[i].contains("Lat"))
                    map.put("Latitude", i);
                else if (line[i].contains("Long"))
                    map.put("Longitude", i);
                else if (line[i].contains("Confirmed"))
                    map.put("Confirmed", i);
                else if (line[i].contains("Deaths"))
                    map.put("Deaths", i);
                else if (line[i].contains("Recovered"))
                    map.put("Recovered", i);
                else if (line[i].contains("date"))
                    map.put("Date", i);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

    void printDB(String data, HashMap<String, Integer> map) {


    }

    private double[] getCoordinates(String province) {
        double[] coordinates = new double[2];
        coordinates[0] = em.createNativeQuery("SELECT p.latitude FROM provincies WHERE p.province=province")
            .getFirstResult();
        coordinates[1] = em.createNativeQuery("SELECT p.longitude FROM provincies WHERE p.province=province")
            .getFirstResult();


        return coordinates;
    }
}





