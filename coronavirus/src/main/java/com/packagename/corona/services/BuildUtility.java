package com.packagename.corona.services;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import com.packagename.corona.models.Report;
import com.packagename.corona.models.Statistic;
import com.packagename.corona.models.WorldStatisticWrapper;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.AxisTitle;
import com.vaadin.flow.component.charts.model.ChartType;
import com.vaadin.flow.component.charts.model.Configuration;
import com.vaadin.flow.component.charts.model.Cursor;
import com.vaadin.flow.component.charts.model.DataLabels;
import com.vaadin.flow.component.charts.model.DataSeries;
import com.vaadin.flow.component.charts.model.DataSeriesItem;
import com.vaadin.flow.component.charts.model.HorizontalAlign;
import com.vaadin.flow.component.charts.model.LayoutDirection;
import com.vaadin.flow.component.charts.model.Legend;
import com.vaadin.flow.component.charts.model.ListSeries;
import com.vaadin.flow.component.charts.model.PlotOptionsLine;
import com.vaadin.flow.component.charts.model.PlotOptionsPie;
import com.vaadin.flow.component.charts.model.RangeSelector;
import com.vaadin.flow.component.charts.model.Tooltip;
import com.vaadin.flow.component.charts.model.VerticalAlign;
import com.vaadin.flow.component.charts.model.YAxis;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class BuildUtility {
    @Inject private DbService dbService;

    public Chart createWeeklyChart(String country) {
        Statistic current = dbService.getCountryStatistic(country);
        Chart chart = new Chart();

        Configuration configuration = chart.getConfiguration();
        configuration.getChart().setType(ChartType.LINE);
        configuration.getChart().setMarginRight(130);
        configuration.getChart().setMarginBottom(25);

        configuration.getTitle().setText("Weekly Pandemic Statistics For " + country);
        configuration.getSubTitle().setText("Source: John Hopkins");

        configuration.getxAxis().setCategories("Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday");

        YAxis yAxis = configuration.getyAxis();
        yAxis.setTitle(new AxisTitle("Number of Cases"));
        yAxis.getTitle().setAlign(VerticalAlign.HIGH);

        configuration.getTooltip().setFormatter(
            "return '<b>'+ this.series.name +'</b><br/>'+this.x +': '+ this.y +' cases'");

        PlotOptionsLine plotOptions = new PlotOptionsLine();
        plotOptions.setDataLabels(new DataLabels(true));
        configuration.setPlotOptions(plotOptions);

        Legend legend = configuration.getLegend();
        legend.setLayout(LayoutDirection.VERTICAL);
        legend.setAlign(HorizontalAlign.RIGHT);
        legend.setVerticalAlign(VerticalAlign.TOP);
        legend.setX(-10d);

        legend.setY(100d);

        Statistic[] statisticList = dbService.getWeeklyStatistics("Greece");
        Number[] total = new Number[7];
        Number[] recovered = new Number[7];
        Number[] death = new Number[7];


        for (int i = 0; i < 7; i++) {
            total[i] = statisticList[i].getConfirmed();
            recovered[i] = statisticList[i].getRecovered();
            death[i] = statisticList[i].getDeaths();
        }

        ListSeries ls = new ListSeries();
        ls.setName("Total");
        ls.setData(total);
        configuration.addSeries(ls);

        ls = new ListSeries();
        ls.setName("Recovered");
        ls.setData(recovered);
        configuration.addSeries(ls);

        ls = new ListSeries();
        ls.setName("Deaths");
        ls.setData(death);
        configuration.addSeries(ls);

        return chart;
    }

    public Chart createPieChart() {
        return new Chart(ChartType.PIE);
    }

    public Chart createLineChart() {
        return new Chart(ChartType.LINE);
    }

    public Chart createSplineChart() {
        return new Chart(ChartType.SPLINE);
    }

    public void setUpPieChart(Chart chart, Statistic statistic) {
        String country = statistic.getReport() != null ? statistic.getReport().getCountry().getCountry() : "World";

        Configuration conf = chart.getConfiguration();
        conf.setTitle("Epidemic Statistics for " + country);
        Tooltip tooltip = new Tooltip();
        tooltip.setValueDecimals(1);
        conf.setTooltip(tooltip);

        PlotOptionsPie plotOptions = new PlotOptionsPie();
        plotOptions.setAllowPointSelect(true);
        plotOptions.setCursor(Cursor.POINTER);
        plotOptions.setShowInLegend(true);
        conf.setPlotOptions(plotOptions);

        DataSeries series = new DataSeries();
        long aux = statistic.getConfirmed() - statistic.getDeaths() - statistic.getRecovered();
        DataSeriesItem active = new DataSeriesItem("Active cases", aux);
        active.setSelected(true);
        active.setSliced(true);
        series.add(active);
        series.add(new DataSeriesItem("Recovered cases", statistic.getRecovered()));
        series.add(new DataSeriesItem("Total deaths", statistic.getDeaths()));

        conf.setSeries(series);
        chart.setVisibilityTogglingDisabled(true);

    }

    public void SetUpWeeklyLineChart(Chart chart, Statistic statistic) {
        String country = statistic.getReport() != null ? statistic.getReport().getCountry().getCountry() : "World";

        Configuration configuration = chart.getConfiguration();
        configuration.getChart().setType(ChartType.LINE);
        configuration.getChart().setMarginRight(130);
        configuration.getChart().setMarginBottom(25);

        configuration.getTitle().setText("Weekly Pandemic Statistics For " + country);
        configuration.getSubTitle().setText("Source: John Hopkins");

        configuration.getxAxis().setCategories("Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday");

        YAxis yAxis = configuration.getyAxis();
        yAxis.setTitle(new AxisTitle("Number of Cases"));
        yAxis.getTitle().setAlign(VerticalAlign.HIGH);

        configuration.getTooltip().setFormatter(
            "return '<b>'+ this.series.name +'</b><br/>'+this.x +': '+ this.y +' cases'");

        PlotOptionsLine plotOptions = new PlotOptionsLine();
        plotOptions.setDataLabels(new DataLabels(true));
        configuration.setPlotOptions(plotOptions);

        Legend legend = configuration.getLegend();
        legend.setLayout(LayoutDirection.VERTICAL);
        legend.setAlign(HorizontalAlign.RIGHT);
        legend.setVerticalAlign(VerticalAlign.TOP);
        legend.setX(-10d);

        legend.setY(100d);

        Statistic[] statisticList = dbService.getWeeklyStatistics(country);
        Number[] total = new Number[7];
        Number[] recovered = new Number[7];
        Number[] death = new Number[7];


        for (int i = 0; i < 7; i++) {
            total[i] = statisticList[i].getConfirmed();
            recovered[i] = statisticList[i].getRecovered();
            death[i] = statisticList[i].getDeaths();
        }

        ListSeries ls = new ListSeries();
        ls.setName("Total");
        ls.setData(total);
        configuration.addSeries(ls);

        ls = new ListSeries();
        ls.setName("Recovered");
        ls.setData(recovered);
        configuration.addSeries(ls);

        ls = new ListSeries();
        ls.setName("Deaths");
        ls.setData(death);
        configuration.addSeries(ls);

    }


    public void setupActiveCasesSplineChart(Chart chart, Statistic statistic) {
        String country = statistic.getReport() != null ? statistic.getReport().getCountry().getCountry() : "World";
        chart.setTimeline(true);

        Configuration configuration = chart.getConfiguration();
        configuration.getTitle().setText("Active cases");
        configuration.getTooltip().setEnabled(true);

        DataSeries dataSeries = new DataSeries();
        List<Report> reports = dbService.getCountryReports(country);
        for (Report report : reports) {
            DataSeriesItem item = new DataSeriesItem();
            item.setX(report.getDate().toInstant());
            item.setY(report.getStatistic().getConfirmed());
            dataSeries.add(item);
        }

        configuration.addSeries(dataSeries);

        RangeSelector rangeSelector = new RangeSelector();
        rangeSelector.setSelected(1);
        configuration.setRangeSelector(rangeSelector);
    }

    public VerticalLayout buildMostAffected() {
        VerticalLayout layout = new VerticalLayout();
        Label lab = new Label("Most affected - based on Confirmed cases");
        List<Statistic> statisticList = dbService.getMostAffected();
        List<WorldStatisticWrapper> result = new ArrayList<>();
        for (Statistic statistic : statisticList) {
            WorldStatisticWrapper wrapper = new WorldStatisticWrapper(statistic);
            result.add(wrapper);

            //countryList.add(statistic.getReport().getCountry());
        }

        Grid<WorldStatisticWrapper> grid = new Grid(WorldStatisticWrapper.class);
        grid.setItems(result);
        grid.setColumns("name", "confirmed", "active", "deaths", "recovered");
        grid.getColumnByKey("name").setHeader("Country");
        grid.setSelectionMode(Grid.SelectionMode.NONE);

        grid.addItemDoubleClickListener(e -> {
            UI.getCurrent().navigate("WorldView/" + e.getItem().getName());
        });

        layout.add(lab, grid);
        return layout;
    }

    public VerticalLayout buildMostHealthy() {
        VerticalLayout layout = new VerticalLayout();
        Label lab = new Label("Most healthy - based on Confirmed cases");
        List<Statistic> statisticList = dbService.getMostHealthy();
        List<WorldStatisticWrapper> result = new ArrayList<>();
        for (Statistic statistic : statisticList) {
            WorldStatisticWrapper wrapper = new WorldStatisticWrapper(statistic);
            result.add(wrapper);
        }

        Grid<WorldStatisticWrapper> grid = new Grid(WorldStatisticWrapper.class);
        grid.setItems(result);
        grid.setColumns("name", "confirmed", "active", "deaths", "recovered");
        grid.getColumnByKey("name").setHeader("Country");

        grid.addItemDoubleClickListener(e -> {
            UI.getCurrent().navigate("WorldView/" + e.getItem().getName());
        });

        layout.add(lab, grid);
        return layout;
    }

    public HorizontalLayout buildNewest() {
        HorizontalLayout mainLayout = new HorizontalLayout();
        VerticalLayout rawConfirmed = new VerticalLayout();
        VerticalLayout percentageConfirmed = new VerticalLayout();
        VerticalLayout rawDeaths = new VerticalLayout();
        VerticalLayout percentageDeaths = new VerticalLayout();

        rawConfirmed.add("Recovered difference\b(From yesterday");
        percentageConfirmed.add("Recovered difference\b(From yesterday");
        rawDeaths.add("Deaths difference\b(From yesterday");
        percentageDeaths.add("Deaths difference\b(From yesterday");

        dbService.mostConfirmedLastDays().forEach((key, value) -> {
            percentageConfirmed.add(new Label(key + " " +
                dbService.mostConfirmedLastDaysPercentage().get(key) + "%"));
        });

        dbService.mostConfirmedLastDays().forEach((key, value) -> {
            rawConfirmed.add(new Label(key + " " +value));
        });

        ///////////////////////////////////////////////////////////////////////////

        dbService.mostDeathsLastDaysPercentage().forEach((key, value) -> {
            percentageDeaths.add(new Label(key + " " +
                dbService.mostDeathsLastDaysPercentage().get(key) + "%"));
        });

        dbService.mostDeathsLastDays().forEach((key, value) -> {
            rawDeaths.add(new Label(key + " " +value));
        });






        mainLayout.add(rawConfirmed ,percentageConfirmed,rawDeaths,percentageDeaths);

        return mainLayout;
    }


}
