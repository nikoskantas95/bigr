package com.packagename.corona.views;

import com.packagename.corona.services.AdminService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Route("Admin")
public class AdminView extends AppLayout {

    @Inject
    private AdminService adminService;
    private Button update = new Button("Update Coronavirus Database");
    private Button delete = new Button("Delete Coronavirus Database");
    private Button updateAll = new Button("Create Coronavirus Database");
    private Button updateCurrentMonth = new Button("Update Coronavirus Database -Last Month-");
    private Button logout = new Button("Logout");
    private VerticalLayout content = new VerticalLayout();
    private ProgressBar progressBar = new ProgressBar();


    @PostConstruct
    public void init() {
//        if (!isUserAuthenticated()) {
//            LoginForm login = new LoginForm();
//            login.addLoginListener(e -> {
//                if (e.getUsername().equals("admin") && e.getPassword().equals("root")) {
//                    VaadinSession.getCurrent().setAttribute("user", "admin");
//                    UI.getCurrent().getPage().reload();
//                } else {
//                    login.setError(true);
//                }
//            });
//
//            content.add(login);
//        } else {
        Tabs tabs = new Tabs();
        Tab homeTab = new Tab("Home");
        Tab worldStatistics = new Tab("World Statistics");
        Tab about = new Tab("About");
        Tab admin = new Tab("Administrator");

        homeTab.addAttachListener(e -> {
            System.out.println("pressed");
        });
        tabs.addSelectedChangeListener(e -> {
            System.out.println(e.getSelectedTab().getLabel());
            switch (tabs.getSelectedTab().getLabel()) {
                case "Home":
                    UI.getCurrent().navigate("");
                    break;
                case "Administrator":
                    UI.getCurrent().navigate("Admin");
                    break;
            }
        });

        updateAll.addClickListener(e -> {
            progressBar.setVisible(true);
            progressBar.setIndeterminate(true);
            adminService.updateFromScratch();
            progressBar.setIndeterminate(false);
        });

        update.addClickListener(e -> {
            adminService.updateFromScratch();
        });

        delete.addClickListener(e -> {
            adminService.updateFromScratch();
        });
        updateCurrentMonth.addClickListener(e->{
            adminService.updateLastMonth();
        });
        logout.addClickListener(e -> {
            logout();
        });


        content.add(update, progressBar, updateAll, delete, updateCurrentMonth, logout);
        setContent(content);
        tabs.add(homeTab, worldStatistics, about, admin);
        addToNavbar(tabs);
        //}
    }

    private boolean isUserAuthenticated() {
        Object s = VaadinSession.getCurrent().getAttribute("user");
        if (VaadinSession.getCurrent().getAttribute("user") == null) {
            return false;
        }
        return true;
    }

    private void logout() {
        VaadinSession.getCurrent().setAttribute("user", null);
        UI.getCurrent().navigate(" ");
    }

}
