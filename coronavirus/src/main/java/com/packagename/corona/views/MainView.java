package com.packagename.corona.views;

import java.util.HashMap;

import com.packagename.corona.models.Statistic;
import com.packagename.corona.services.AdminService;
import com.packagename.corona.services.BuildUtility;
import com.packagename.corona.services.DbService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Route("")
@CssImport("styles.css")
public class MainView extends AppLayout {

    @Inject
    private DbService dbService;

    @Inject
    private BuildUtility buildUtility;

    @Inject
    private AdminService adminService;

    private Chart pieChart;

    @PostConstruct
    public void init() {


        Tabs tabs = new Tabs();
        Tab homeTab = new Tab("Home");
        Tab worldStatistics = new Tab("World Statistics");
        Tab about = new Tab("About");
        Tab admin = new Tab("Administrator");
        //  tabs.setSelectedTab(homeTab);

        tabs.addSelectedChangeListener(e -> {
            System.out.println(e.getSelectedTab().getLabel());
            switch (tabs.getSelectedTab().getLabel()) {
                case "Administrator":
                    UI.getCurrent().navigate("Admin");
                    break;
                case "World Statistics":
                    UI.getCurrent().navigate("WorldView");
                    break;
                case "WHO":
                    UI.getCurrent().navigate("WhoView");
            }
        });

        Statistic current = dbService.getWorldStatistic();

        VerticalLayout mainStatistics = new VerticalLayout();

        Label total = new Label("Total cases: " + current.getConfirmed());
        Label recovered = new Label("Recovered: " + current.getRecovered());
        Label deaths = new Label("Deaths: " + current.getDeaths());

        pieChart = buildUtility.createPieChart();
        buildUtility.setUpPieChart(pieChart, current);

        VerticalLayout averageLayout = new VerticalLayout();
        VerticalLayout healthyLayout = buildUtility.buildMostHealthy();
        VerticalLayout affectedLayout = buildUtility.buildMostAffected();


       averageLayout.add(buildUtility.buildNewest());

        dbService.mostConfirmedLastDaysPercentage();

        averageLayout.add(pieChart);


        mainStatistics.add(total, recovered, deaths, averageLayout, affectedLayout, healthyLayout);

        setContent(mainStatistics);
        tabs.add(homeTab, worldStatistics, about, admin);
        if (VaadinSession.getCurrent().getAttribute("user") == null)
            tabs.remove(admin);
        tabs.setSelectedTab(homeTab);
        addToNavbar(tabs);
    }


}
