package com.packagename.corona.views;

import com.packagename.corona.models.Country;
import com.packagename.corona.models.Province;
import com.packagename.corona.models.Statistic;
import com.packagename.corona.services.BuildUtility;
import com.packagename.corona.services.DbService;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.WildcardParameter;
import com.vaadin.flow.server.VaadinSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Route(value = "WorldView")
@CssImport("styles.css")
public class WorldView extends AppLayout implements HasUrlParameter<String>, BeforeEnterObserver {
    @Inject
    private DbService dbService;

    @Inject
    private BuildUtility buildUtility;

    private Chart pieChart;

    private Chart lineChart;

    private Chart splineChart;

    private VerticalLayout mainStatistics = new VerticalLayout();

    private ComboBox<Country> countriesBox = new ComboBox<>();

    private String defaultCountry = "Greece";

    private boolean tabLock = false;


    @PostConstruct
    public void init() {
        Tabs tabs = new Tabs();
        Tab homeTab = new Tab("Home");
        Tab worldStatistics = new Tab("World Statistics");
        Tab about = new Tab("About");
        Tab admin = new Tab("Administrator");

        tabs.addSelectedChangeListener(e -> {
            System.out.println(e.getSelectedTab().getLabel());
            switch (tabs.getSelectedTab().getLabel()) {
                case "Home":
                    UI.getCurrent().navigate("");
                    break;
                case "Administrator":
                    UI.getCurrent().navigate("Admin");
                    break;
                case "WHO":
                    UI.getCurrent().navigate("WhoView");

            }
        });


        Statistic current = dbService.getCountryStatistic(defaultCountry);


        countriesBox.setItems(dbService.getCountriesList());
        countriesBox.setLabel("Country");
        countriesBox.setItemLabelGenerator(Country::getCountry);


        countriesBox.addValueChangeListener(e -> {
            renew(e.getValue().getCountry());
//            Statistic newStatistic = dbService.getCountryStatistic(e.getValue().getCountry());
//            buildUtility.setUpPieChart(pieChart, newStatistic);
//            updateMainStatistics(newStatistic);


        });
        updateMainStatistics(current);
        setContent(mainStatistics);
        tabs.add(homeTab, worldStatistics, about, admin);
        if (VaadinSession.getCurrent().getAttribute("user") == null)
            tabs.remove(admin);
        tabs.setSelectedTab(worldStatistics);
        addToNavbar(tabs);



    }

    private void updateMainStatistics(Statistic current) {
        mainStatistics.removeAll();

        Label header = new Label("Statistics for " + current.getReport().getCountry().getCountry());
        Label total = new Label("Total cases: " + current.getConfirmed());
        Label recovered = new Label("Recovered: " + current.getRecovered());
        Label deaths = new Label("Deaths: " + current.getDeaths());

        pieChart = buildUtility.createPieChart();
        lineChart = buildUtility.createLineChart();
        splineChart = buildUtility.createSplineChart();

        buildUtility.setUpPieChart(pieChart, current);
        buildUtility.SetUpWeeklyLineChart(lineChart, current);
        buildUtility.setupActiveCasesSplineChart(splineChart, current);

        mainStatistics.add(header, total, recovered, deaths, countriesBox, pieChart, lineChart, splineChart);

    }


    @Override
    public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
        if (parameter != null) {
            defaultCountry = parameter;
            renew(parameter);
        }
    }

    private void renew(String parameter) {
        Statistic newStatistic = dbService.getCountryStatistic(parameter);
        buildUtility.setUpPieChart(pieChart, newStatistic);
        updateMainStatistics(newStatistic);
    }


    @Override
    public void beforeEnter(BeforeEnterEvent event) {

    }
}


