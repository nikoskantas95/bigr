package com.packagename.corona.views;

import javax.annotation.PostConstruct;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

@Route("login")
public class LoginView extends VerticalLayout {
    LoginForm login = new LoginForm();


    public LoginView() {
        login.addLoginListener(e -> {
            if (e.getUsername().equals("admin") && e.getPassword().equals("root")) {
                VaadinSession.getCurrent().setAttribute("user", "admin");
                UI.getCurrent().navigate("Admin");
            } else {
                login.setError(true);
            }
        });

        this.add(login);
    }
}
