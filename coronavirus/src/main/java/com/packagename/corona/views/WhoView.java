package com.packagename.corona.views;

import javax.annotation.PostConstruct;

import com.packagename.corona.components.TestDiv;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;

@Route(value = "WhoView")
public class WhoView extends AppLayout {
    @PostConstruct
    public void init() {
        Tabs tabs = new Tabs();
        Tab homeTab = new Tab("Home");
        Tab worldStatistics = new Tab("World Statistics");
        Tab who = new Tab("WHO");
        Tab about = new Tab("About");
        Tab admin = new Tab("Administrator");

        tabs.addSelectedChangeListener(e -> {
            System.out.println(e.getSelectedTab().getLabel());
            switch (tabs.getSelectedTab().getLabel()) {
                case "Home":
                    UI.getCurrent().navigate("");
                    break;
                case "Administrator":
                    UI.getCurrent().navigate("Admin");
                    break;
                case "World Statistics":
                    UI.getCurrent().navigate("WorldView");
                    break;
                case "WHO":
                    UI.getCurrent().navigate("WhoView");
            }
        });
        setContent(new TestDiv());
    }
}
