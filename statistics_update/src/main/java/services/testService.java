package services;

import com.opencsv.CSVReader;
import models.City;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.FileReader;
import java.io.IOException;

public class testService {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void add(City city){
        em.persist(city);
    }

    @Transactional
    public void testCreateCities(){
        String csvFile = "C:/Users/Nikos/Documents/shared/Ptyxiaki/raw_data/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/03-17-2020.csv";
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
//            line=reader.readNext();
//            System.out.println("Country : " + line[0]);
            while ((line = reader.readNext()) != null) {
                System.out.println("Country : " + line[0]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
