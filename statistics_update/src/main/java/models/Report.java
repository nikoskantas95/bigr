package models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "Reports")
public class Report {



    @Id
    private Long id;

    @Column
    private Date date;

    @ManyToOne
    private City city;

    @OneToOne
    private Statistic statistic;
}
