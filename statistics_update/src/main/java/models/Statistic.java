package models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "Statistics")
public class Statistic {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long confirmed;

    @Column
    private Long Deaths;

    @Column
    private Long Recovered;

    @OneToOne
    City city;
}
